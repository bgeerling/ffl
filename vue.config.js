const webpack = require('webpack')
require('dotenv').config()

module.exports = {
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        CLIENT_ID: JSON.stringify(process.env.CLIENT_ID)
      })
    ]
  },
  css: {
    loaderOptions: {
      sass: {
        implementation: require('sass')
      }
    }
  },
  devServer: {
    /*
    netsh interface portproxy add v4tov4 listenport=80 listenaddress=127.0.0.1 connectport=8080 connectaddress=127.0.0.1
    netsh interface portproxy add v4tov4 listenport=443 listenaddress=127.0.0.1 connectport=8080 connectaddress=127.0.0.1
    netsh interface portproxy delete v4tov4 listenport=80 listenaddress=127.0.0.1
    netsh interface portproxy delete v4tov4 listenport=443 listenaddress=127.0.0.1
    netsh interface portproxy show v4tov4
    */
    host: 'lvh.me',
    historyApiFallback: true,
    disableHostCheck: true,
    https: {
      key: process.env.key,
      cert: process.env.cert
    },
    proxy: {
      '/fantasy': {
        target: 'https://fantasysports.yahooapis.com',
        ws: true,
        changeOrigin: true
      }
    }
  }
}
