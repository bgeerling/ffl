import { expect } from 'chai'
import fs from 'fs'
import { parseString } from 'xml2js'
import { transformTop } from '../../../src/store/transform'

// function transformTop (top) {
//   return top.fantasy_content.league.players.player.reduce((acc, player) => {
//     acc.allIds.push(player.player_key)
//     Object.assign(acc.byId, {
//       [player.player_key]: {
//         player_key: player.player_key,
//         name: player.name.full,
//         points: player.player_points.total,
//         display_position: player.display_position,
//         editorial_team_abbr: player.editorial_team_abbr
//       }
//     })
//     return acc
//   }, { byId: {}, allIds: [] })
// }

let top
parseString(fs.readFileSync('public/api/top.xml', 'UTF-8'), { explicitArray: false }, (err, result) => {
  if (err !== null) {
    throw err
  }
  top = result
})

describe('Transform top', () => {
  it('sets transform properties', () => {
    const players = transformTop(top)

    expect(players).to.have.property('allIds')
    expect(players).to.have.property('byId')
  })
  it('sets player properties', () => {
    const players = transformTop(top)
    const player0 = players.byId[players.allIds[0]]

    expect(player0).to.have.property('player_key')
    expect(player0).to.have.property('name')
    expect(player0).to.have.property('points')
    expect(player0).to.have.property('display_position')
    expect(player0).to.have.property('editorial_team_abbr')
  })
  // it('mergeTop has same count as top', () => {
  //   const players = mergeTop(top, roster)
  //   expect(players.length).to.deep.equal(top.fantasy_content.league.players.player.length)
  // })
  // it('matches players to is_owned_by_current_login', () => {
  //   const myTeam = roster.fantasy_content.league.teams.team.find(team => ('is_current_login' in team.managers.manager))
  //   const players = mergeTop(top, roster)
  //   const player = players.find(player => player.team_key === myTeam.team_key)

  //   expect(player.is_owned_by_current_login).to.deep.equal(true)
  // })
  // it('does not match players when not is_owned_by_current_login', () => {
  //   const myTeam = roster.fantasy_content.league.teams.team.find(team => !('is_current_login' in team.managers.manager))

  //   const players = mergeTop(top, roster)
  //   const player = players.find(player => player.team_key === myTeam.team_key)

  //   expect(player.is_owned_by_current_login).to.deep.equal(false)
  // })
})
