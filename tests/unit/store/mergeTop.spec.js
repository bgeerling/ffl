import { expect } from 'chai'
import fs from 'fs'
import { parseString } from 'xml2js'
import { transformTop, transformRosters } from '../../../src/store/transform'

let top
parseString(fs.readFileSync('public/api/top.xml', 'UTF-8'), { explicitArray: false }, (err, result) => {
  if (err !== null) {
    throw err
  }
  top = result
})

let roster
parseString(fs.readFileSync('public/api/league-roster.xml', 'UTF-8'), { explicitArray: false }, (err, result) => {
  if (err !== null) {
    throw err
  }
  roster = result
})

describe('Left join top eq roster on player_key where manager is_current_login', () => {
  const players = transformTop(top)
  const rosters = transformRosters(roster)
  const currentLoginTeamKey = rosters.teams.allIds.find(teamKey => rosters.teams.byId[teamKey].is_current_login)

  let topPlayersOnRoster = players.allIds.reduce((acc, playerKey) => {
    acc.allIds.push(playerKey)
    Object.assign(acc.byId, {
      [playerKey]: {
        ...players.byId[playerKey],
        ...rosters.players.byId[playerKey],
        is_owned_by_current_login: rosters.teams.byId[currentLoginTeamKey].players.some(key => key === playerKey),
        position: (rosters.players.byId[playerKey] !== undefined) ? rosters.players.byId[playerKey].selected_position : players.byId[playerKey].display_position
      }
    })
    return acc
  }, { byId: {}, allIds: [] })

  it('unmatched top player sets properties', () => {
    const unmatchedlayerKey = players.allIds.find(playerKey => (rosters.players.byId[playerKey] === undefined))
    const player0 = topPlayersOnRoster.byId[unmatchedlayerKey]
    // expect(player0).to.have.property('team_key')
    // expect(player0).to.have.property('is_owned_by_current_login')
    // expect(player0).to.have.property('selected_position')
    expect(player0).to.have.property('is_owned_by_current_login')
    expect(player0).to.have.property('position')

    expect(player0).to.have.property('player_key')
    expect(player0).to.have.property('name')
    expect(player0).to.have.property('points')
    expect(player0).to.have.property('display_position')
    expect(player0).to.have.property('editorial_team_abbr')
  })

  it('matched top player sets properties', () => {
    const matchedlayerKey = players.allIds.find(playerKey => (rosters.players.byId[playerKey] !== undefined))
    const player0 = topPlayersOnRoster.byId[matchedlayerKey]
    // expect(player0).to.have.property('team_key')
    expect(player0).to.have.property('is_owned_by_current_login')
    expect(player0).to.have.property('position')

    expect(player0).to.have.property('player_key')
    expect(player0).to.have.property('name')
    expect(player0).to.have.property('points')
    expect(player0).to.have.property('display_position')
    expect(player0).to.have.property('editorial_team_abbr')
  })
  it('matched top player where manager is_current_login sets properties', () => {
    const matchedlayerKey = rosters.teams.byId[currentLoginTeamKey].players[0]
    const player0 = topPlayersOnRoster.byId[matchedlayerKey]
    // expect(player0).to.have.property('team_key')
    expect(player0.is_owned_by_current_login).to.be.equal(true)
  })
})
