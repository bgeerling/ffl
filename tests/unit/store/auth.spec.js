import { expect } from 'chai'

describe('Check token', () => {
  it('authenticated null token returns false ', () => {
    const expected = false

    const token = null
    const authenticated = !!token

    expect(authenticated).to.deep.equal(expected)
  })
  it('authenticated token returns true ', () => {
    const expected = true

    const token = 'abc'
    const authenticated = !!token

    expect(authenticated).to.deep.equal(expected)
  })
})
