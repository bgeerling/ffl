import { expect } from 'chai'
import { getWeeks } from '../../../src/store/transform'

describe('Get weeks', () => {
  it('returns expected', () => {
    const expected = ['1', '2', '3', '4', '5']
    const weeks = getWeeks(5)
    expect(weeks).to.deep.equal(expected)
  })
})
