import { expect } from 'chai'
import fs from 'fs'
import { parseString } from 'xml2js'
import { transformRosters } from '../../../src/store/transform'

let rosters
parseString(fs.readFileSync('public/api/league-roster.xml', 'UTF-8'), { explicitArray: false }, (err, result) => {
  if (err !== null) {
    throw err
  }
  rosters = result
})

describe('Transform rosters', () => {
  const transformedRosters = transformRosters(rosters)

  it('transform has properties', () => {
    expect(transformedRosters.teams).to.have.property('byId')
    expect(transformedRosters.teams).to.have.property('allIds')
    expect(transformedRosters.players).to.have.property('byId')
    expect(transformedRosters.players).to.have.property('allIds')
  })
  it('teams have expected count', () => {
    expect(transformedRosters.teams.allIds.length).to.deep.equal(rosters.fantasy_content.league.teams.team.length)
    expect(Object.keys(transformedRosters.teams.byId).length).to.deep.equal(rosters.fantasy_content.league.teams.team.length)
  })
  it('team has properties', () => {
    const team0 = transformedRosters.teams.byId[transformedRosters.teams.allIds[0]]
    expect(team0).to.have.property('team_key')
    expect(team0).to.have.property('name')
    expect(team0).to.have.property('players')
    expect(team0).to.have.property('manager_key')
    expect(team0).to.have.property('is_current_login')
  })
  it('managers have expected count', () => {
    expect(transformedRosters.managers.allIds.length).to.deep.equal(rosters.fantasy_content.league.teams.team.length)
    expect(Object.keys(transformedRosters.managers.byId).length).to.deep.equal(rosters.fantasy_content.league.teams.team.length)
  })
  it('managers has properties', () => {
    const manager0 = transformedRosters.managers.byId[transformedRosters.managers.allIds[0]]
    expect(manager0).to.have.property('manager_key')
    expect(manager0).to.have.property('name')
    // expect(manager0).to.have.property('is_current_login')
  })
  it('players have expected count', () => {
    let expected = rosters.fantasy_content.league.teams.team.reduce((acc, team) => {
      return acc + team.roster.players.player.reduce(acc => acc + 1, 0)
    }, 0)
    expect(transformedRosters.players.allIds.length).to.deep.equal(expected)
    expect(Object.keys(transformedRosters.players.byId).length).to.deep.equal(expected)
  })
  it('player has properties', () => {
    const player0 = transformedRosters.players.byId[transformedRosters.players.allIds[0]]
    expect(player0).to.have.property('player_key')
    expect(player0).to.have.property('name')
    expect(player0).to.have.property('selected_position')
  })
  it('team joins players by managers', () => {
    const team0 = transformedRosters.teams.byId[transformedRosters.teams.allIds[0]]
    const manager = transformedRosters.managers.byId[team0.manager_key]
    expect(manager).to.have.property('manager_key')
    expect(manager).to.have.property('name')
    // expect(manager).to.have.property('is_current_login')
  })
  it('team joins players by roster', () => {
    const team0 = transformedRosters.teams.byId[transformedRosters.teams.allIds[0]]
    const player0 = transformedRosters.players.byId[team0.players[0]]
    expect(player0).to.have.property('player_key')
    expect(player0).to.have.property('name')
    expect(player0).to.have.property('selected_position')
  })
})
