import { expect } from 'chai'
import fs from 'fs'
import { parseString } from 'xml2js'
import { transformMatchups } from '../../../src/store/transform'

let response = fs.readFileSync('public/api/scoreboard-week.xml', 'UTF-8')
let data = {}
parseString(response, { explicitArray: false }, (err, result) => {
  if (err !== null) {
    throw err
  }
  data = result
})

describe('Get data from matchups', () => {
  it('matchup has properties', () => {
    const matchups = transformMatchups(data)
    let matchup0 = matchups[0]
    expect(matchup0).to.have.property('status')
  })
  it('team has properties', () => {
    const matchups = transformMatchups(data)
    let team0 = matchups[0].teams[0]
    expect(team0).to.have.property('team_key')
    expect(team0).to.have.property('name')
    expect(team0).to.have.property('is_current_login')
    expect(team0).to.have.property('points')
  })
  // it('find current login matchup', () => {
  //   let myMatchup = matchups.matchup.find(matchup => {
  //     let manager = matchup.teams[0].team[0].managers[0].manager[0]
  //     manager.is_current_login = ('is_current_login' in manager)
  //     if (manager.is_current_login === true) {
  //       return matchup
  //     }
  //   })
  //   let manager = myMatchup.teams[0].team[0].managers[0].manager[0]
  //   expect(manager.nickname[0]).to.be.deep.equal('Ben')
  // })
})
