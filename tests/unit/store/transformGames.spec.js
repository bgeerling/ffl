import { expect } from 'chai'
import fs from 'fs'
import { parseString } from 'xml2js'
import { transformGames } from '../../../src/store/transform'

let games
parseString(fs.readFileSync('public/api/games-league.xml', 'UTF-8'), { explicitArray: false }, (err, result) => {
  if (err !== null) {
    throw err
  }
  games = result
})

describe('Transform games', () => {
  const transformedGames = transformGames(games)

  it('has default properties', () => {
    expect(transformedGames).to.have.property('byId')
    expect(transformedGames).to.have.property('allIds')
  })
  it('has expected count', () => {
    const expected = games.fantasy_content.users.user.games.game.leagues.league.length
    expect(transformedGames.allIds.length).to.deep.equal(expected)
    expect(Object.keys(transformedGames.byId).length).to.deep.equal(expected)
  })
  it('has properties', () => {
    const league0 = transformedGames.byId[transformedGames['allIds'][0]]

    expect(league0).to.have.property('league_key')
    expect(league0).to.have.property('league_name')
    expect(league0).to.have.property('current_week')
    expect(league0).to.have.property('team_key')
    expect(league0).to.have.property('team_name')
  })
})
