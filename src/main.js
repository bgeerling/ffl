// import dotenv from 'dotenv'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/main'
import './registerServiceWorker'
import './assets/style.scss'
import '@fortawesome/fontawesome-free/css/all.css'

// dotenv.config()
router.beforeEach((routeTo, routeFrom, next) => {
  // if (routeFrom.name) {
  //   // this.$state.commit('loading', true)
  // }

  const authRequired = routeTo.matched.some(route => route.meta.authRequired)

  if (!authRequired) return next()

  if (store.isAuthenticated) {
    return next()
  } else {
    store.dispatch('login', 'https://lvh.me' + routeTo.fullPath)
    next()
  }
})

Vue.config.productionTip = false

Vue.filter('toFixed', function (value, decimal) {
  return value.toFixed(decimal)
})

Vue.filter('toPercent', function (value) {
  return (value * 100).toString() + '%'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
