import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import HomeView from './components/HomeView.vue'
import QueryView from './components/QueryView.vue'
import GameView from './components/GameView.vue'
import MatchupView from './components/MatchupView.vue'

// import auth from '@/store/auth'

Vue.use(Router)
Vue.use(Meta, {
  keyName: 'page'
})

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/query',
      meta: {
        authRequired: true
      },
      component: QueryView
    },
    {
      path: '/games',
      meta: {
        authRequired: true
      },
      component: GameView
    },
    {
      name: 'matchups',
      path: '/matchups/:selectedLeagueKey?/:selectedWeek?',
      meta: {
        authRequired: true
      },
      component: MatchupView,
      props: true
    }
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})

// router.scrollBehavior((to, from, savedPosition) => {
//   if (savedPosition) {
//     return savedPosition
//   } else {
//     return { x: 0, y: 0 }
//   }
// })

export default router
