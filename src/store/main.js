import Vue from 'vue'
import Vuex from 'vuex'
import { parseString } from 'xml2js'
import { transformGames, getWeeks, transformRosters, transformMatchups, transformTop, mergeTop } from './transform'
import auth from './auth'
import settings from './settings'

Vue.use(Vuex)

let defaultResource = function (selectedLeagueKey, selectedWeek) {
  return {
    games: '/fantasy/v2/users;use_login=1/games;is_available=1;game_keys=nfl/leagues/teams',
    roster: `/fantasy/v2/league/${selectedLeagueKey}/teams/roster;week=${selectedWeek}`,
    scoreboard: `/fantasy/v2/league/${selectedLeagueKey}/scoreboard;week=${selectedWeek}`,
    top: `/fantasy/v2/league/${selectedLeagueKey}/players;count=25;sort=PTS;sort_type=week;sort_week=${selectedWeek};start=0/stats;type=week;week=${selectedWeek}`
  }
}

// let auth = {
//   login (redirectURI) { throw new Error('not implemented') },
//   isAuthenticated () {},
//   fetch (url) { throw new Error('not implemented') },
//   logout () {}
// }

// if (process.env.NODE_ENV === 'development') {
// import fetch from 'unfetch'
//   auth.fetch = fetch

//   defaultResource = () => {
//     return {
//       games: '/api/games-league.xml',
//       roster: '/api/league-roster.xml',
//       scoreboard: '/api/scoreboard-week.xml',
//       top: '/api/top.xml'
//     }
//   }
// }

export default new Vuex.Store({
  modules: {
    settings
  },
  getters: {
    isAuthenticated () { return auth.isAuthenticated() },
    defaultLeagueKey: state => {
      if (localStorage.selectedLeagueKey !== undefined) {
        return localStorage.selectedLeagueKey
      } else if (state.games !== null) {
        return state.games.byId[state.games.allIds[0]].league_key
      }
    },
    defaultWeek: state => {
      if (localStorage.selectedWeek !== undefined) {
        return localStorage.selectedWeek
      } else if (state.games !== null) {
        return state.games.byId[state.games.allIds[0]].current_week
      }
    }
  },
  state: {
    url: (localStorage.selectedLeagueKey !== undefined) ? `/fantasy/v2/league/${localStorage.selectedLeagueKey}/` : null,
    data: null,
    loading: true,
    // form: defaultForm,
    games: null,
    selectedDraftStatus: 'predraft',
    selectedLeagueKey: (localStorage.selectedLeagueKey !== undefined) ? localStorage.selectedLeagueKey : null,
    selectedWeek: (localStorage.selectedWeek !== undefined) ? localStorage.selectedWeek : null,
    weeks: null,
    matchups: [{
      status: '',
      teams: [{
        name: '',
        is_current_login: false,
        points: {
          projected: 0,
          total: 0
        }
      },
      {
        name: '',
        is_current_login: false,
        points: {
          projected: 0,
          total: 0
        }
      }]
    }],
    resource: defaultResource(),
    scoreboard: {
      fantasy_content: {
        league: {
          scoreboard: {
            week: 0,
            matchups: {
              matchup: [{
                status: '',
                teams: {
                  team: [{
                    name: '',
                    team_points: {
                      total: 0
                    },
                    managers: {
                      manager: {}
                    }
                  },
                  {
                    name: '',
                    team_points: {
                      total: 0
                    },
                    managers: {
                      manager: {}
                    }
                  }]
                }
              }]
            }
          }
        }
      }
    },
    roster: null,
    top: null
  },
  mutations: {
    url (state, url) {
      state.url = url
    },
    data (state, data) {
      state.data = data
    },
    loading (state, loading) {
      state.loading = loading
    },
    form (state, form) {
      state.form = form
    },
    resource (state, resource) {
      state.resource = resource
    },
    games (state, games) {
      state.games = games
    },
    selectedDraftStatus (state, selectedDraftStatus) {
      state.selectedDraftStatus = selectedDraftStatus
    },
    selectedLeagueKey (state, selectedLeagueKey) {
      state.selectedLeagueKey = selectedLeagueKey
      localStorage.selectedLeagueKey = selectedLeagueKey
    },
    selectedWeek (state, selectedWeek) {
      state.selectedWeek = selectedWeek
      localStorage.selectedWeek = selectedWeek
    },
    weeks (state, weeks) {
      state.weeks = weeks
    },
    team (state, team) {
      state.team = team
    },
    matchups (state, matchups) {
      state.matchups = matchups
    },
    scoreboard (state, scoreboard) {
      state.scoreboard = scoreboard
    },
    roster (state, roster) {
      state.roster = roster
    },
    top (state, top) {
      state.top = top
    }
  },
  actions: {
    login (redirectURI) {
      auth.login(redirectURI)
    },
    fetch (url) {
      auth.fetch(url)
    },
    get ({ state, commit }) {
      auth.fetch(state.url)
        .then(response => response.text())
        .then(data => {
          commit('data', data)
        })
        .catch(err => console.log(err))
    },
    selected ({ commit }, selected) {
      return new Promise((resolve, reject) => {
        if (selected) {
          if ('selectedLeagueKey' in selected) {
            commit('selectedLeagueKey', selected.selectedLeagueKey)
            localStorage.selectedLeagueKey = selected.selectedLeagueKey
          }
          if ('selectedWeek' in selected) {
            commit('selectedWeek', selected.selectedWeek)
            localStorage.selectedWeek = selected.selectedWeek
          }
          resolve(selected)
        } else {
          reject(new Error('no selection'))
        }
      })
    },
    games ({ state, commit }) {
      return new Promise((resolve, reject) => {
        if (state.games) {
          resolve(state.games)
        }
        auth.fetch(state.resource.games)
          .then(response => response.text())
          .then(data => {
            parseString(data, { explicitArray: false }, (err, result) => {
              if (err !== null) {
                reject(err)
              }
              let transformedGames = transformGames(result)
              if (transformedGames['allIds'].length > 0) {
                commit('games', transformedGames)
                commit('weeks', getWeeks(transformedGames.byId[transformedGames.allIds[0]].current_week))
                resolve(state.games)
              } else {
                reject(new Error('no games'))
              }
            })
          })
      })
    },
    roster ({ state, commit }) {
      return new Promise((resolve, reject) => {
        auth.fetch(state.resource.roster)
          .then(response => response.text())
          .then(data => {
            parseString(data, { explicitArray: false }, (err, result) => {
              if (err !== null) {
                reject(err)
              }
              const transformedRoster = transformRosters(result)
              commit('roster', transformedRoster)
              resolve(state.roster)
            })
          })
      })
    },
    top ({ state, commit }) {
      return new Promise((resolve, reject) => {
        auth.fetch(state.resource.top)
          .then(response => response.text())
          .then(data => {
            parseString(data, { explicitArray: false }, (err, result) => {
              if (err !== null) {
                reject(err)
              }
              const topplayers = transformTop(result)
              commit('top', mergeTop(topplayers, state.roster))
              resolve()
            })
          })
      })
    },
    scoreboard ({ state, commit }) {
      auth.fetch(state.resource.scoreboard)
        .then(response => response.text())
        .then(data => {
          parseString(data, { explicitArray: false }, (err, result) => {
            if (err !== null) {
              throw err
            }
            commit('matchups', transformMatchups(result))
          })
        })
    },
    changeLeague ({ commit }, payload) {
      commit('selectedLeagueKey', payload.selectedLeagueKey)
    },
    changeWeek ({ commit }, payload) {
      commit('selectedWeek', payload.selectedWeek)
    }
    // changeLeague ({ dispatch, commit }, payload) {
    //   commit('loading', true)
    //   commit('selectedLeagueKey', payload.selectedLeagueKey)
    //   commit('selectedWeek', payload.selectedWeek)
    //   // dispatch('selected', selected)
    //   commit('resource', defaultResource(payload.selectedLeagueKey, payload.selectedWeek))
    //   Vue.nextTick(() => {
    //     dispatch('scoreboard')
    //     dispatch('roster')
    //       .then(() => {
    //         dispatch('top')
    //       })
    //       .then(commit('loading', false))
    //   })
    // }
  }
})
