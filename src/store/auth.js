import { JSO, Fetcher } from 'jso'

export default {
  login (redirectURI) {
    this.yahoo = new JSO({
      providerID: 'yahoo',
      // response_type: 'code',
      // eslint-disable-next-line
      client_id: CLIENT_ID,
      redirect_uri: 'https://lvh.me',
      authorization: 'https://api.login.yahoo.com/oauth2/request_auth',
      scopes: { request: ['openid', 'fspt-r'] }
    })
    this.yahoo.callback()
    this.fetcher = new Fetcher(this.yahoo)
  },
  isAuthenticated () {
    return !!this.yahoo.checkToken()
  },
  fetch (url) {
    return this.fetcher.fetch(url)
  },
  logout () {
    this.yahoo.wipeTokens()
  }
}
