export default {
  state: () => ({
    draft_type: null,
    roster_positions: [{
      position: null,
      is_starting_position: null,
      count: 0
    }],
    stats: [{
      stat_id: 0,
      enabled: 0,
      name: null,
      display_name: null,
      sort_order: 0
    }]
  }),
  mutations: {
    settings (state, settings) {
      state.settings = settings
    }
  }
}

export function transformSettings (settings) {
  let rosterPositions = settings.fantasy_content.league.settings.roster_positions.roster_position.reduce((acc, rosterPosition) => {
    return Object.assign(acc.byId, {
      [player.player_key]: {
        player_key: player.player_key,
        name: player.name.full,
        points: player.player_points.total,
        display_position: player.display_position,
        editorial_team_abbr: player.editorial_team_abbr
      }
    })
  }, { })

  let stats = settings.fantasy_content.league.settings.stats.stat.reduce((acc, stat) => {
    return Object.assign(acc.byId, {
      [stat.stat_id]: {
        stat_id: stat.stat_id,
        enabled: stat.enabled,
        name: stat.name,
        display_name: stat.display_name,
        sort_order: stat.sort_order
      }
    })
  }, { })

  return settings.fantasy_content.league.settings.reduce((acc, setting) => {
    Object.assign(acc.byId, {
      [player.player_key]: {
        player_key: player.player_key,
        name: player.name.full,
        points: player.player_points.total,
        display_position: player.display_position,
        editorial_team_abbr: player.editorial_team_abbr
      }
    })
    return acc
  }, { })
}

/*
<?xml version="1.0" encoding="UTF-8"?>
<fantasy_content xml:lang="en-US" yahoo:uri="http://fantasysports.yahooapis.com/fantasy/v2/league/414.l.305634/settings" time="25.804996490479ms" copyright="Data provided by Yahoo! and STATS, LLC" refresh_rate="60" xmlns:yahoo="http://www.yahooapis.com/v1/base.rng" xmlns="http://fantasysports.yahooapis.com/fantasy/v2/base.rng">
 <league>
  <league_key>414.l.305634</league_key>
  <league_id>305634</league_id>
  <name>F.F.L.</name>
  <url>https://football.fantasysports.yahoo.com/f1/305634</url>
  <logo_url></logo_url>
  <draft_status>predraft</draft_status>
  <num_teams>12</num_teams>
  <edit_key>1</edit_key>
  <weekly_deadline/>
  <league_update_timestamp/>
  <scoring_type>head</scoring_type>
  <league_type>private</league_type>
  <renew>406_183602</renew>
  <renewed/>
  <felo_tier>gold</felo_tier>
  <iris_group_chat_id/>
  <allow_add_to_dl_extra_pos>0</allow_add_to_dl_extra_pos>
  <is_pro_league>0</is_pro_league>
  <is_cash_league>0</is_cash_league>
  <current_week>1</current_week>
  <start_week>1</start_week>
  <start_date>2022-09-08</start_date>
  <end_week>17</end_week>
  <end_date>2023-01-02</end_date>
  <game_code>nfl</game_code>
  <season>2022</season>
  <settings>
   <draft_type>self</draft_type>
   <is_auction_draft>0</is_auction_draft>
   <scoring_type>head</scoring_type>
   <persistent_url>https://football.fantasysports.yahoo.com/league/ffl3003</persistent_url>
   <uses_playoff>1</uses_playoff>
   <has_playoff_consolation_games>1</has_playoff_consolation_games>
   <playoff_start_week>15</playoff_start_week>
   <uses_playoff_reseeding>0</uses_playoff_reseeding>
   <uses_lock_eliminated_teams>0</uses_lock_eliminated_teams>
   <num_playoff_teams>6</num_playoff_teams>
   <num_playoff_consolation_teams>6</num_playoff_consolation_teams>
   <has_multiweek_championship>0</has_multiweek_championship>
   <uses_roster_import>1</uses_roster_import>
   <roster_import_deadline>2022-09-07</roster_import_deadline>
   <waiver_type>R</waiver_type>
   <waiver_rule>gametime</waiver_rule>
   <uses_faab>0</uses_faab>
   <draft_pick_time>60</draft_pick_time>
   <post_draft_players>W</post_draft_players>
   <max_teams>12</max_teams>
   <waiver_time>1</waiver_time>
   <trade_end_date>2022-11-19</trade_end_date>
   <trade_ratify_type>vote</trade_ratify_type>
   <trade_reject_time>1</trade_reject_time>
   <player_pool>ALL</player_pool>
   <cant_cut_list>yahoo</cant_cut_list>
   <draft_together>0</draft_together>
   <can_trade_draft_picks>1</can_trade_draft_picks>
   <sendbird_channel_url>b48e07698c0cbcc92497ca396ca21672</sendbird_channel_url>
   <roster_positions>
    <roster_position>
     <position>QB</position>
     <position_type>O</position_type>
     <count>1</count>
     <is_starting_position>1</is_starting_position>
    </roster_position>
    <roster_position>
     <position>WR</position>
     <position_type>O</position_type>
     <count>3</count>
     <is_starting_position>1</is_starting_position>
    </roster_position>
    <roster_position>
     <position>RB</position>
     <position_type>O</position_type>
     <count>2</count>
     <is_starting_position>1</is_starting_position>
    </roster_position>
    <roster_position>
     <position>TE</position>
     <position_type>O</position_type>
     <count>1</count>
     <is_starting_position>1</is_starting_position>
    </roster_position>
    <roster_position>
     <position>W/R/T</position>
     <position_type>O</position_type>
     <count>1</count>
     <is_starting_position>1</is_starting_position>
    </roster_position>
    <roster_position>
     <position>K</position>
     <position_type>K</position_type>
     <count>1</count>
     <is_starting_position>1</is_starting_position>
    </roster_position>
    <roster_position>
     <position>DEF</position>
     <position_type>DT</position_type>
     <count>1</count>
     <is_starting_position>1</is_starting_position>
    </roster_position>
    <roster_position>
     <position>BN</position>
     <count>7</count>
     <is_starting_position>0</is_starting_position>
    </roster_position>
   </roster_positions>
   <stat_categories>
    <stats>
     <stat>
      <stat_id>2</stat_id>
      <enabled>1</enabled>
      <name>Completions</name>
      <display_name>Comp</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>4</stat_id>
      <enabled>1</enabled>
      <name>Passing Yards</name>
      <display_name>Pass Yds</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>5</stat_id>
      <enabled>1</enabled>
      <name>Passing Touchdowns</name>
      <display_name>Pass TD</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>6</stat_id>
      <enabled>1</enabled>
      <name>Interceptions</name>
      <display_name>Int</display_name>
      <sort_order>0</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>7</stat_id>
      <enabled>1</enabled>
      <name>Sacks</name>
      <display_name>Sack</display_name>
      <sort_order>0</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>8</stat_id>
      <enabled>1</enabled>
      <name>Rushing Attempts</name>
      <display_name>Rush Att</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
        <is_only_display_stat>1</is_only_display_stat>
       </stat_position_type>
      </stat_position_types>
      <is_only_display_stat>1</is_only_display_stat>
     </stat>
     <stat>
      <stat_id>9</stat_id>
      <enabled>1</enabled>
      <name>Rushing Yards</name>
      <display_name>Rush Yds</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>10</stat_id>
      <enabled>1</enabled>
      <name>Rushing Touchdowns</name>
      <display_name>Rush TD</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>78</stat_id>
      <enabled>1</enabled>
      <name>Targets</name>
      <display_name>Targets</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
        <is_only_display_stat>1</is_only_display_stat>
       </stat_position_type>
      </stat_position_types>
      <is_only_display_stat>1</is_only_display_stat>
     </stat>
     <stat>
      <stat_id>11</stat_id>
      <enabled>1</enabled>
      <name>Receptions</name>
      <display_name>Rec</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>12</stat_id>
      <enabled>1</enabled>
      <name>Receiving Yards</name>
      <display_name>Rec Yds</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>13</stat_id>
      <enabled>1</enabled>
      <name>Receiving Touchdowns</name>
      <display_name>Rec TD</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>15</stat_id>
      <enabled>1</enabled>
      <name>Return Touchdowns</name>
      <display_name>Ret TD</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>16</stat_id>
      <enabled>1</enabled>
      <name>2-Point Conversions</name>
      <display_name>2-PT</display_name>
      <sort_order>1</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>18</stat_id>
      <enabled>1</enabled>
      <name>Fumbles Lost</name>
      <display_name>Fum Lost</display_name>
      <sort_order>0</sort_order>
      <position_type>O</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>O</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>19</stat_id>
      <enabled>1</enabled>
      <name>Field Goals 0-19 Yards</name>
      <display_name>FG 0-19</display_name>
      <sort_order>1</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>20</stat_id>
      <enabled>1</enabled>
      <name>Field Goals 20-29 Yards</name>
      <display_name>FG 20-29</display_name>
      <sort_order>1</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>21</stat_id>
      <enabled>1</enabled>
      <name>Field Goals 30-39 Yards</name>
      <display_name>FG 30-39</display_name>
      <sort_order>1</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>22</stat_id>
      <enabled>1</enabled>
      <name>Field Goals 40-49 Yards</name>
      <display_name>FG 40-49</display_name>
      <sort_order>1</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>23</stat_id>
      <enabled>1</enabled>
      <name>Field Goals 50+ Yards</name>
      <display_name>FG 50+</display_name>
      <sort_order>1</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>24</stat_id>
      <enabled>1</enabled>
      <name>Field Goals Missed 0-19 Yards</name>
      <display_name>FGM 0-19</display_name>
      <sort_order>0</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>25</stat_id>
      <enabled>1</enabled>
      <name>Field Goals Missed 20-29 Yards</name>
      <display_name>FGM 20-29</display_name>
      <sort_order>0</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>26</stat_id>
      <enabled>1</enabled>
      <name>Field Goals Missed 30-39 Yards</name>
      <display_name>FGM 30-39</display_name>
      <sort_order>0</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>27</stat_id>
      <enabled>1</enabled>
      <name>Field Goals Missed 40-49 Yards</name>
      <display_name>FGM 40-49</display_name>
      <sort_order>0</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>28</stat_id>
      <enabled>1</enabled>
      <name>Field Goals Missed 50+ Yards</name>
      <display_name>FGM 50+</display_name>
      <sort_order>0</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>29</stat_id>
      <enabled>1</enabled>
      <name>Point After Attempt Made</name>
      <display_name>PAT Made</display_name>
      <sort_order>1</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>30</stat_id>
      <enabled>1</enabled>
      <name>Point After Attempt Missed</name>
      <display_name>PAT Miss</display_name>
      <sort_order>0</sort_order>
      <position_type>K</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>K</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>31</stat_id>
      <enabled>1</enabled>
      <name>Points Allowed</name>
      <display_name>Pts Allow</display_name>
      <sort_order>0</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
        <is_only_display_stat>1</is_only_display_stat>
       </stat_position_type>
      </stat_position_types>
      <is_only_display_stat>1</is_only_display_stat>
     </stat>
     <stat>
      <stat_id>32</stat_id>
      <enabled>1</enabled>
      <name>Sack</name>
      <display_name>Sack</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>33</stat_id>
      <enabled>1</enabled>
      <name>Interception</name>
      <display_name>Int</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>34</stat_id>
      <enabled>1</enabled>
      <name>Fumble Recovery</name>
      <display_name>Fum Rec</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>35</stat_id>
      <enabled>1</enabled>
      <name>Touchdown</name>
      <display_name>TD</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>36</stat_id>
      <enabled>1</enabled>
      <name>Safety</name>
      <display_name>Safe</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>37</stat_id>
      <enabled>1</enabled>
      <name>Block Kick</name>
      <display_name>Blk Kick</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>49</stat_id>
      <enabled>1</enabled>
      <name>Kickoff and Punt Return Touchdowns</name>
      <display_name>Ret TD</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>50</stat_id>
      <enabled>1</enabled>
      <name>Points Allowed 0 points</name>
      <display_name>Pts Allow 0</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>51</stat_id>
      <enabled>1</enabled>
      <name>Points Allowed 1-6 points</name>
      <display_name>Pts Allow 1-6</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>52</stat_id>
      <enabled>1</enabled>
      <name>Points Allowed 7-13 points</name>
      <display_name>Pts Allow 7-13</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>53</stat_id>
      <enabled>1</enabled>
      <name>Points Allowed 14-20 points</name>
      <display_name>Pts Allow 14-20</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>54</stat_id>
      <enabled>1</enabled>
      <name>Points Allowed 21-27 points</name>
      <display_name>Pts Allow 21-27</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>55</stat_id>
      <enabled>1</enabled>
      <name>Points Allowed 28-34 points</name>
      <display_name>Pts Allow 28-34</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>56</stat_id>
      <enabled>1</enabled>
      <name>Points Allowed 35+ points</name>
      <display_name>Pts Allow 35+</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
     </stat>
     <stat>
      <stat_id>82</stat_id>
      <enabled>1</enabled>
      <name>Extra Point Returned</name>
      <display_name>XPR</display_name>
      <sort_order>1</sort_order>
      <position_type>DT</position_type>
      <stat_position_types>
       <stat_position_type>
        <position_type>DT</position_type>
       </stat_position_type>
      </stat_position_types>
      <is_excluded_from_display>1</is_excluded_from_display>
     </stat>
    </stats>
   </stat_categories>
   <stat_modifiers>
    <stats>
     <stat>
      <stat_id>2</stat_id>
      <value>.25</value>
     </stat>
     <stat>
      <stat_id>4</stat_id>
      <value>0.04</value>
      <bonuses>
       <bonus>
        <target>350</target>
        <points>5</points>
       </bonus>
      </bonuses>
     </stat>
     <stat>
      <stat_id>5</stat_id>
      <value>6</value>
     </stat>
     <stat>
      <stat_id>6</stat_id>
      <value>-2</value>
     </stat>
     <stat>
      <stat_id>7</stat_id>
      <value>-1</value>
     </stat>
     <stat>
      <stat_id>9</stat_id>
      <value>0.1</value>
      <bonuses>
       <bonus>
        <target>150</target>
        <points>5</points>
       </bonus>
      </bonuses>
     </stat>
     <stat>
      <stat_id>10</stat_id>
      <value>6</value>
     </stat>
     <stat>
      <stat_id>11</stat_id>
      <value>1</value>
     </stat>
     <stat>
      <stat_id>12</stat_id>
      <value>0.1</value>
      <bonuses>
       <bonus>
        <target>150</target>
        <points>5</points>
       </bonus>
      </bonuses>
     </stat>
     <stat>
      <stat_id>13</stat_id>
      <value>6</value>
     </stat>
     <stat>
      <stat_id>15</stat_id>
      <value>6</value>
     </stat>
     <stat>
      <stat_id>16</stat_id>
      <value>2</value>
     </stat>
     <stat>
      <stat_id>18</stat_id>
      <value>-2</value>
     </stat>
     <stat>
      <stat_id>19</stat_id>
      <value>3</value>
     </stat>
     <stat>
      <stat_id>20</stat_id>
      <value>3</value>
     </stat>
     <stat>
      <stat_id>21</stat_id>
      <value>3</value>
     </stat>
     <stat>
      <stat_id>22</stat_id>
      <value>4</value>
     </stat>
     <stat>
      <stat_id>23</stat_id>
      <value>5</value>
     </stat>
     <stat>
      <stat_id>24</stat_id>
      <value>-5</value>
     </stat>
     <stat>
      <stat_id>25</stat_id>
      <value>-4</value>
     </stat>
     <stat>
      <stat_id>26</stat_id>
      <value>-3</value>
     </stat>
     <stat>
      <stat_id>27</stat_id>
      <value>-2</value>
     </stat>
     <stat>
      <stat_id>28</stat_id>
      <value>-1</value>
     </stat>
     <stat>
      <stat_id>29</stat_id>
      <value>1</value>
     </stat>
     <stat>
      <stat_id>30</stat_id>
      <value>-2</value>
     </stat>
     <stat>
      <stat_id>32</stat_id>
      <value>1</value>
     </stat>
     <stat>
      <stat_id>33</stat_id>
      <value>2</value>
     </stat>
     <stat>
      <stat_id>34</stat_id>
      <value>2</value>
     </stat>
     <stat>
      <stat_id>35</stat_id>
      <value>6</value>
     </stat>
     <stat>
      <stat_id>36</stat_id>
      <value>2</value>
     </stat>
     <stat>
      <stat_id>37</stat_id>
      <value>2</value>
     </stat>
     <stat>
      <stat_id>49</stat_id>
      <value>6</value>
     </stat>
     <stat>
      <stat_id>50</stat_id>
      <value>10</value>
     </stat>
     <stat>
      <stat_id>51</stat_id>
      <value>7</value>
     </stat>
     <stat>
      <stat_id>52</stat_id>
      <value>4</value>
     </stat>
     <stat>
      <stat_id>53</stat_id>
      <value>1</value>
     </stat>
     <stat>
      <stat_id>54</stat_id>
      <value>0</value>
     </stat>
     <stat>
      <stat_id>55</stat_id>
      <value>-1</value>
     </stat>
     <stat>
      <stat_id>56</stat_id>
      <value>-4</value>
     </stat>
     <stat>
      <stat_id>82</stat_id>
      <value>2</value>
     </stat>
    </stats>
   </stat_modifiers>
   <divisions>
    <division>
     <division_id>1</division_id>
     <name>WEED</name>
    </division>
    <division>
     <division_id>2</division_id>
     <name>EDIBLE</name>
    </division>
    <division>
     <division_id>3</division_id>
     <name>LIQUOR</name>
    </division>
    <division>
     <division_id>4</division_id>
     <name>BEER</name>
    </division>
   </divisions>
   <pickem_enabled>1</pickem_enabled>
   <uses_fractional_points>0</uses_fractional_points>
   <uses_negative_points>1</uses_negative_points>
  </settings>
 </league>
</fantasy_content>
<!-- fantasy-sports-api- -public-production-bf1-98f5db568-h68hj Sun Aug  7 13:03:54 UTC 2022 -->

*/
