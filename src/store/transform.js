export function getWeeks (currentWeek) {
  let weeks = []
  let lastWeek = Number.parseInt(currentWeek)
  for (let i = 1; i <= lastWeek; i++) {
    weeks.push(i.toString())
  }
  return weeks
}

export function transformTop (top) {
  return top.fantasy_content.league.players.player.reduce((acc, player) => {
    if (player.player_points.total > 0) {
      acc.allIds.push(player.player_key)
      Object.assign(acc.byId, {
        [player.player_key]: {
          player_key: player.player_key,
          name: player.name.full,
          points: player.player_points.total,
          display_position: player.display_position,
          editorial_team_abbr: player.editorial_team_abbr
        }
      })
    }
    return acc
  }, { byId: {}, allIds: [] })
}

export function mergeTop (players, rosters) {
  const currentLoginTeamKey = rosters.teams.allIds.find(teamKey => rosters.teams.byId[teamKey].is_current_login)
  return players.allIds.reduce((acc, playerKey) => {
    acc.allIds.push(playerKey)
    Object.assign(acc.byId, {
      [playerKey]: {
        ...players.byId[playerKey],
        ...rosters.players.byId[playerKey],
        is_owned_by_current_login: rosters.teams.byId[currentLoginTeamKey].players.some(key => key === playerKey),
        position: (rosters.players.byId[playerKey] !== undefined) ? rosters.players.byId[playerKey].selected_position : players.byId[playerKey].display_position
      }
    })
    return acc
  }, { byId: {}, allIds: [] })
}

export function transformGames (games) {
  return games.fantasy_content.users.user.games.game.leagues.league.reduce((acc, league) => {
    acc.allIds.push(league.league_key)
    Object.assign(acc.byId, {
      [league.league_key]: {
        league_key: league.league_key,
        league_name: league.name,
        draft_status: league.draft_status,
        team_key: league.teams.team.team_key,
        team_name: league.teams.team.name,
        current_week: league.current_week,
        season: league.season
      }
    })
    return acc
  }, { byId: {}, allIds: [] })
}

export function transformMatchups (scoreboard) {
  return scoreboard.fantasy_content.league.scoreboard.matchups.matchup.map(matchup => {
    return {
      status: matchup.status,
      teams: matchup.teams.team.map(team => {
        return {
          name: team.name,
          team_key: team.team_key,
          is_current_login: ('is_current_login' in team.managers.manager),
          win_probability: (Number(team.win_probability) * 100).toFixed(0),
          points: {
            projected: Number(team.team_projected_points.total),
            total: Number(team.team_points.total)
          },
          grade: ('matchup_grades' in matchup) ? matchup.matchup_grades.matchup_grade.find(matchupGrade => matchupGrade.team_key === team.team_key).grade : ''
        }
      })
    }
  })
}

export function transformRosters (rosters) {
  // const week = rosters.fantasy_content.league.current_week
  // if (rosters === undefined) {

  // }
  let teams = rosters.fantasy_content.league.teams.team.reduce((acc, team) => {
    let players = team.roster.players.player.reduce((acc, player) => {
      return Object.assign(acc, {
        [player.player_key]: {
          player_key: player.player_key,
          name: player.name.full,
          selected_position: player.selected_position.position
        }
      })
    }, {})

    acc.players.allIds.push(...Object.keys(players))
    Object.assign(acc.players.byId, players)

    acc.managers.allIds.push(team.managers.manager.guid)
    Object.assign(acc.managers.byId, {
      [team.managers.manager.guid]: {
        manager_key: team.managers.manager.guid,
        name: team.managers.manager.nickname
      }
    })

    acc.teams.allIds.push(team.team_key)
    Object.assign(acc.teams.byId, {
      [team.team_key]: {
        team_key: team.team_key,
        name: team.name,
        is_current_login: ('is_current_login' in team.managers.manager),
        manager_key: team.managers.manager.guid,
        players: Object.keys(players)
      }
    })
    return acc
  }, { teams: { byId: {}, allIds: [] }, managers: { byId: {}, allIds: [] }, players: { byId: {}, allIds: [] } })

  return teams
}
